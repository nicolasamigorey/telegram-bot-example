class HereResponse
  def self.handle_distance_response(response)
    if response.success?
      json_response = JSON.parse(response.body)
      if !json_response['notices'].nil?
        'Origen o destino inexistente, intente nuevamente'
      else
        duration = json_response['routes'][0]['sections'][0]['summary']['duration']
        "Vas a tardar #{duration / 60} minutos en hacer tu viaje en auto"
      end
    else
      'Error conectando a servidor, intente nuevamente'
    end
  end
end
